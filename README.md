# Unity FMOD (FSB5) Audio Extractor
This program extracts FMOD's FSB5-encoded audio bundles from raw asset files in UnityFS format.

May not work with all audio files. (Only tested with OGG files from [Re: Tree of Savior](https://mobile.nexon.co.jp/retos))

## How to use
1. Install UnityPy from PyPI with `pip install UnityPy`
2. Create folders `extracted` and `raw` in repo root.
3. Put raw AssetBundle files in `raw` folder.
4. Run `python run.py` from shell.

Tested with Python 3.7.4.

## Credits
- [python-fsb5](https://github.com/HearthSim/python-fsb5)
  - Since the version on PyPI was outdated and was causing ogg decode errors, the latest GitHub version is included in `python-fsb5` folder. (Commit `5acfaed` at the time of writing.)
- [UnityPy](https://github.com/K0lb3/UnityPy)