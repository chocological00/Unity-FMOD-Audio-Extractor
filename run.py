from pathlib import Path
from os import listdir

# use the version on git, not the one on pypi
# import fsb5
from python_fsb5 import fsb5

import UnityPy

files = [f for f in listdir('raw')]

def extractPackage(input, output, filename):
	env = UnityPy.load(input)
	for obj in env.objects:
		if obj.type == 'TextAsset':
			data = obj.read()
			s = data.script.tobytes()
			test = s.find(b'\x46\x53\x42\x35') # magic number for "FSB5"
			chopped = s[test:]
			fsb = fsb5.FSB5(chopped)
			print(fsb.header)

			ext = fsb.get_sample_extension()
			if len(fsb.samples) > 1:
				output = output + f'/{filename}'
			Path(output).mkdir(parents=True, exist_ok=True)
			for sample in fsb.samples:
				print('''\t{sample.name}.{extension}:
				Frequency: {sample.frequency}
				Channels: {sample.channels}
				Samples: {sample.samples}'''.format(sample=sample, extension=ext))
				with open(f'{output}/{sample.name}.{ext}', 'wb') as f:
					rebuilt_sample = fsb.rebuild_sample(sample)
					f.write(rebuilt_sample)


for file in files:
	print(file)
	extractPackage(f'raw/{file}', 'extracted', file)